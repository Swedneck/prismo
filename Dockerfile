FROM node:8.11.3-alpine as node
FROM ruby:2.5.1-alpine3.7

LABEL maintainer="https://gitlab.com/mbajur/prismo" \
      description="Federated link aggregation powered by ActivityPub"

ARG UID=991
ARG GID=991

ENV PATH=/prismo/bin:$PATH \
    RAILS_SERVE_STATIC_FILES=true \
    RAILS_ENV=production \
    NODE_ENV=production

ARG LIBICONV_VERSION=1.15
ARG LIBICONV_DOWNLOAD_SHA256=ccf536620a45458d26ba83887a983b96827001e92a13847b45e4925cc8913178

EXPOSE 3000 4000

WORKDIR /prismo

COPY --from=node /usr/local/bin/node /usr/local/bin/node
COPY --from=node /usr/local/lib/node_modules /usr/local/lib/node_modules
COPY --from=node /usr/local/bin/npm /usr/local/bin/npm
COPY --from=node /opt/yarn-* /opt/yarn

RUN apk -U upgrade \
 && apk add -t build-dependencies \
    build-base \
    icu-dev \
    libidn-dev \
    libressl \
    libtool \
    postgresql-dev \
    protobuf-dev \
    python \
 && apk add \
    ca-certificates \
    file \
    git \
    icu-libs \
    imagemagick \
    libidn \
    libpq \
    protobuf \
    tini \
    tzdata \
 && update-ca-certificates \
 && ln -s /opt/yarn/bin/yarn /usr/local/bin/yarn \
 && ln -s /opt/yarn/bin/yarnpkg /usr/local/bin/yarnpkg \
 && mkdir -p /tmp/src /opt \
 && wget -O libiconv.tar.gz "https://ftp.gnu.org/pub/gnu/libiconv/libiconv-$LIBICONV_VERSION.tar.gz" \
 && echo "$LIBICONV_DOWNLOAD_SHA256 *libiconv.tar.gz" | sha256sum -c - \
 && tar -xzf libiconv.tar.gz -C /tmp/src \
 && rm libiconv.tar.gz \
 && cd /tmp/src/libiconv-$LIBICONV_VERSION \
 && ./configure --prefix=/usr/local \
 && make -j$(getconf _NPROCESSORS_ONLN)\
 && make install \
 && libtool --finish /usr/local/lib \
 && cd /prismo \
 && rm -rf /tmp/* /var/cache/apk/*

COPY Gemfile Gemfile.lock package.json yarn.lock /prismo/

RUN bundle config build.nokogiri --with-iconv-lib=/usr/local/lib --with-iconv-include=/usr/local/include \
 && bundle install -j$(getconf _NPROCESSORS_ONLN) --deployment --without test development \
 && yarn install --pure-lockfile --ignore-engines \
 && yarn cache clean

RUN addgroup -g ${GID} prismo && adduser -h /prismo -s /bin/sh -D -G prismo -u ${UID} prismo \
 && mkdir -p /prismo/public/system /prismo/public/assets /prismo/public/packs \
 && chown -R prismo:prismo /prismo/public

COPY . /prismo

RUN chown -R prismo:prismo /prismo

VOLUME /prismo/public/system

USER prismo

RUN SECRET_KEY_BASE=precompile_placeholder bundle exec rails webpacker:compile

ENTRYPOINT ["/sbin/tini", "--"]

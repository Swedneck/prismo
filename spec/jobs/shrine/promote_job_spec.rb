require "rails_helper"

describe Shrine::PromoteJob do
  let(:data) { {} }

  describe '#perform' do
    subject { described_class.new.send(:perform, data.stringify_keys) }

    before do
      allow(Shrine::Attacher).to receive(:promote).and_return(true)
    end

    it 'calls shrine promoter' do
      expect(Shrine::Attacher).to receive(:promote).with(data.stringify_keys)
      subject
    end

    context 'when uploader is StoryThumbUploader' do
      let(:story) { Fabricate(:story) }
      let(:data) { { shrine_class: 'StoryThumbUploader', record: ['Story', story.id] } }

      it 'broadcasts story updates notification' do
        expect(Stories::BroadcastChanges).to receive(:run!)
        subject
      end
    end
  end
end

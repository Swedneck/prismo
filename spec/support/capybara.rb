require "capybara-webkit"
require "capybara/rails"
require "capybara/rspec"
require 'capybara/email/rspec'
require 'capybara-screenshot/rspec'
require "selenium-webdriver"

Capybara.register_driver :selenium do |app|
  Capybara::Selenium::Driver.new(
    app,
    browser: :chrome,
    args: ["--window-size=1440,900"]
  )
end

Capybara::Screenshot.prune_strategy = :keep_last_run
Capybara::Screenshot.webkit_options = { width: 1440, height: 900 }

# That's a hack to make capybara open emails in a proper host/port environment
Prismo::Application.config.action_mailer.default_url_options[:host] = "127.0.0.1:#{Capybara.server_port}"
Capybara.always_include_port = true

Capybara.configure do |config|
  config.default_driver = :webkit
  config.javascript_driver = :selenium
  config.default_max_wait_time = 10
end

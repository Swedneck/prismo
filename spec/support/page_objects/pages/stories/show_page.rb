require_relative '../../sections/navbar_section'
require_relative '../../sections/comments_tree_section'
require_relative '../../sections/story_row_section'

class Stories::ShowPage < SitePrism::Page
  set_url '/stories{/id}'
  set_url_matcher %r{\/stories\/\d+\z}

  section :navbar, NavbarSection, '.navbar-main'
  section :comments_tree, CommentsTreeSection, '[data-controller="comments-tree"]'
  sections :stories, StoryRowSection, '.stories-list li.story-row'
end

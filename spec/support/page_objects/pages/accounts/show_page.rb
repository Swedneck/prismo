require_relative '../../sections/story_row_section'
require_relative '../../sections/account_sub_nav_section'

class Accounts::ShowPage < SitePrism::Page
  set_url '/@{username}'
  set_url_matcher %r{\/@\w+\z}

  section :sub_nav, AccountSubNavSection, '.account-head .tab'

  sections :stories, StoryRowSection, '.stories-list li.story-row'
end

class StoryRowSection < SitePrism::Section
  element :root, '.story-row'
  element :vote_button, '.story-row-score-btn'
  element :votes_count, '.story-row-score-votes-count'

  def click_vote_button
    vote_button.click
  end

  def voted?
    root_element[:'data-voted'] == 'true'
  end
end

require 'rails_helper'

feature 'Creating sub-comment' do
  let(:story_page) { Stories::ShowPage.new }
  let(:sign_in_page) { SignInPage.new }

  let(:user) { create(:user, :with_account, password: 'TestPass') }
  let!(:story) { create(:story) }
  let!(:comment) { create(:comment, story: story) }

  def sign_user_in
    sign_in_page.load
    sign_in_page.sign_in_using(user.email, 'TestPass')
  end

  scenario 'signed in user wants to vote on comment', js: true do
    sign_user_in
    story_page.load(id: story.id)

    # Check if story page has all the commenting-related sections
    expect(story_page).to be_displayed
    expect(story_page).to have_stories
    expect(story_page).to have_comments_tree
    expect(story_page.comments_tree).to have_root_comment_form

    expect(story_page.comments_tree).to have_comments
    first_comment = story_page.comments_tree.comments.first

    first_comment.click_vote_button
    sleep 2

    # Check value and unvote
    comment_el = story_page.comments_tree.comments.first
    expect(comment_el.votes_count.text).to eq '1 votes'
    comment_el.click_vote_button
    sleep 2

    # Check value and upvote
    comment_el = story_page.comments_tree.comments.first
    expect(comment_el.votes_count.text).to eq '0 votes'
    comment_el.click_vote_button
    sleep 2

    # Check value
    comment_el = story_page.comments_tree.comments.first
    expect(comment_el.votes_count.text).to eq '1 votes'
  end
end

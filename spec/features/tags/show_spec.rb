require 'rails_helper'

feature 'Browsing tag page' do
  let(:sign_in_page) { SignInPage.new }
  let(:tag_page) { Tags::ShowPage.new }
  let(:tag_comments_page) { Tags::CommentsPage.new }

  let(:user) { create(:user, :with_account) }
  let!(:story) { create(:story, account: user.account) }
  let!(:tag) { story.tag_names.last }

  def sign_user_in
    sign_in_page.load
    sign_in_page.sign_in_using(user.email, 'TestPass')
  end

  def it_behaves_like_stories_page
    expect(tag_page).to be_displayed
    expect(tag_page).to have_heading
    expect(tag_page.heading).to have_content tag
    expect(tag_page).to have_stories
    expect(tag_page.stories.length).to eq 1
  end

  scenario 'guest user browses tag homepage' do
    tag_page.load(name: tag)
    it_behaves_like_stories_page
  end

  scenario 'signed in user browses tag homepage' do
    sign_user_in
    tag_page.load(name: tag)
    it_behaves_like_stories_page
  end
end

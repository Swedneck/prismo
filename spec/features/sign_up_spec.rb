require 'rails_helper'

feature 'Signing up' do
  let(:sign_up_page) { SignUpPage.new }
  let(:sign_in_page) { SignInPage.new }
  let(:home_page) { HomePage.new }

  let(:user) { Fabricate(:user, password: 'TestPass', confirmed_at: Time.zone.now) }

  before do
    sign_up_page.load
  end

  scenario 'signing up with fully valid credentials' do
    sign_up_page.username_field.set 'newuser'
    sign_up_page.email_field.set 'newuser@example.com'
    sign_up_page.password_field.set 'TestPass'
    sign_up_page.password_confirmation_field.set 'TestPass'
    sign_up_page.submit_button.click

    expect(home_page).to be_displayed
    # @todo: test if success message is displayed

    open_email('newuser@example.com')
    current_email.click_link 'Confirm my account'

    expect(sign_in_page).to be_displayed
    expect(sign_in_page).to have_content 'Your email address has been successfully confirmed'
  end

  scenario 'signing up with username taken localy' do
    sign_up_page.username_field.set user.account.username
    sign_up_page.email_field.set 'newuser@example.com'
    sign_up_page.password_field.set 'TestPass'
    sign_up_page.password_confirmation_field.set 'TestPass'
    sign_up_page.submit_button.click

    expect(sign_up_page).to be_displayed
    expect(sign_in_page).to have_content 'Username has already been taken'
  end

  scenario 'signing up with username taken remotely' do
    remote_account = Fabricate(:account, domain: 'example.com')

    sign_up_page.username_field.set remote_account.username
    sign_up_page.email_field.set 'newuser@example.com'
    sign_up_page.password_field.set 'TestPass'
    sign_up_page.password_confirmation_field.set 'TestPass'
    sign_up_page.submit_button.click

    expect(home_page).to be_displayed
    # @todo: test if success message is displayed
  end
end

require 'faker'

FactoryBot.define do
  factory :comment do
    body Faker::Lovecraft.paragraph

    account
    story
  end
end

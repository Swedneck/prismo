FactoryBot.define do
  factory :account do
    sequence(:username) { |i| "username#{i}" }
  end
end

# ▵ Prismo  [![pipeline status](https://gitlab.com/mbajur/prismo/badges/master/pipeline.svg)](https://gitlab.com/mbajur/prismo/commits/master) [![coverage report](https://gitlab.com/mbajur/prismo/badges/master/coverage.svg)](https://gitlab.com/mbajur/prismo/commits/master)

Federated link aggregation powered by ActivityPub.

![Screenshot](https://files.mastodon.social/media_attachments/files/004/376/046/original/751727c9ce7ec570.png)

***

## Getting started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

- ruby 2.5.1
- node.js 10.6.0
- yarn 1.7.0
- postgresql 10

### Installing

A step by step series of examples that tell you have to get a development env running.

#### Setup / first run

If that's a first time you're installing the app, start with installing dependencies:

    $ bundle install
    $ yarn install

Setup database

    $ bundle exec rails db:setup

#### Development

If app has been set up already and you want to continue working on it:

    $ bundle exec rails db:migrate
    $ foreman start

## Running the tests

Running unit specs:

    $ bundle exec rspec

## Deployment

### Prerequisites

- Working basic (Linux) server with Nginx (or Apache2; not officially supported).
- Recent stable version of Docker.
- Recent stable version of Docker-compose.

### Setting up

Clone Prismo repository

    git clone https://gitlab.com/mbajur/prismo.git
    cd prismo

Review the settings in `docker-compose.yml`. Note that it is *not default* to store the postgresql database and redis databases in a persistent storage location. If you plan on running your instance in production, you must uncomment the volumes directive in `docker-compose.yml`.

### Getting the Prismo image

#### Using a prebuilt image

If you're not making any local code changes or customizations on your instance, you can use a prebuilt Docker image to avoid the time and resource consumption of a build. Images are available from gitlab registry: https://gitlab.com/mbajur/prismo/container_registry

To use the prebuilt images:

1. Open `docker-compose.yml`
    1. Comment out the `build: .` lines for all images (web, sidekiq).
    2. Edit the `image: registry.gitlab.com/mbajur/prismo` lines for all images to include the release you want. The default is `latest` which is the most recent stable version, however it is recommended to explicitly pin a version: If you wanted to use v2.2.0 for example, you would edit the lines to say: `image: registry.gitlab.com/mbajur/prismo:v2.2.0`
   3. Save the file and exit the text editor.
2. Run `cp .env.production.sample .env.production` to bootstrap the configuration. You will need to edit this file later.
3. Run `docker-compose build`. It will now pull the correct image from Docker Hub.
4. Set correct file-owner with `chown -R 991:991 public`

#### Building your own image

You must build your own image if you've made any code modifications. To build your own image:

1. Open `docker-compose.yml` in your favorite text editor.
    1. Uncomment the `build: .` lines for all images (web, sidekiq) if needed.
    2. Save the file and exit the text editor.
2. Run `cp .env.production.sample .env.production` to bootstrap the configuration. You will need to edit this file later.
3. Run `docker-compose build`.
4. Set correct file-owner with `chown -R 991:991 public`

### Building the app

Open `.env.production` file and edit the configs. Next, run the following commands:

    docker-compose run --rm web bundle exec rails db:create
    docker-compose run --rm web bundle exec rails db:migrate
    docker-compose run --rm web bundle exec rails webpacker:compile

These commands will setup the database schema and precompile assets. Now is the time to create an admin user:

    docker-compose run --rm web bundle exec rake prismo:create_admin

After it's done, you can launch Prismo with:

    docker-compose up -d

Following that, you have a Prismo instance running on 127.0.0.1:3000. **It's highly recommended to serve it using a http proxy like nginx or apache.**

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/mbajur/prismo/tags).


## License

Prismo
Copyright (C) 2018 mbajur

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

import ActionCable from 'actioncable'

const ws = ActionCable.createConsumer("ws://localhost:3000/cable")

export default ws

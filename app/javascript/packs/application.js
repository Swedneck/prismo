import { Application } from "stimulus"
import { definitionsFromContext } from "stimulus/webpack-helpers"
import Turbolinks from "turbolinks"
import axios from 'axios'
import Rails from 'rails-ujs'
import errorStatusResolver from './lib/error_status_resolver'

// Load spectre.css
import './css/application.scss'

// Init rails UJS
Rails.start()

// Init turbolinks
Turbolinks.start()

// Init axios
const csrfEl = document.querySelector("meta[name=csrf-token]")
const csrfToken = csrfEl ? csrfEl.content : null
axios.defaults.headers.common['X-CSRF-Token'] = csrfToken

axios.interceptors.response.use((response) => {
  return response;
}, (error) => {
  errorStatusResolver(error.response.status)
  return Promise.reject(error)
})

// Init app
const application = Application.start()
const context = require.context("./controllers", true, /\.js$/)
application.load(definitionsFromContext(context))

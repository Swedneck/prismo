import BaseController from './base_controller'
import axios from 'axios'

// @todo: Rename that to root_comment_form and reorganize views as, right now,
//   it doesn't make much sens when named as "first_comment" because it also
//   handles logic for a general root comment form.
export default class extends BaseController {
  static targets = ['empty']

  showForm (e) {
    let req = axios.get(e.target.dataset.actionPath)

    req.then((resp) => {
      this.emptyTarget.outerHTML = resp.data
    })
  }
}

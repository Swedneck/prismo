import BaseController from "./base_controller"
import axios from 'axios'

export default class extends BaseController {
  static targets = ['voteBtn']

  toggleVote () {
    let req = axios.post(this.voteBtnTarget.dataset.actionPath)

    req.then((resp) => {
      this.element.outerHTML = resp.data
    })
  }

  scrap (e) {
    e.preventDefault()
    let req = axios.post(e.target.dataset.actionPath)

    req.then(() => {
      this.addToast({ text: 'Story queued for a re-scrap', severity: 'success' })
    })
  }
}

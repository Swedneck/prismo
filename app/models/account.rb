class Account < ApplicationRecord
  include AccountFinderConcern

  include AccountAvatarUploader[:avatar]

  USERNAME_RE = /[a-z0-9_]+([a-z0-9_\.]+[a-z0-9_]+)?/i

  has_one :user, inverse_of: :account
  has_many :votes

  validates :username, presence: true

  # Remote user validations
  validates :username, uniqueness: { scope: :domain, case_sensitive: true }, if: -> { !local? }

  # Local user validations
  validates :username, format: { with: /\A[a-z0-9_]+\z/i },
                       length: { maximum: 30 }, if: -> { local? },
                       uniqueness: { scope: :domain, case_sensitive: true }
  validates :display_name, length: { maximum: 30 }, if: -> { local? }

  scope :remote, -> { where.not(domain: nil) }
  scope :local, -> { where(domain: nil) }
  scope :expiring, ->(time) { remote.where.not(subscription_expires_at: nil).where('subscription_expires_at < ?', time) }
  scope :matches_username, ->(value) { where(arel_table[:username].matches("#{value}%")) }
  scope :matches_display_name, ->(value) { where(arel_table[:display_name].matches("#{value}%")) }
  scope :matches_domain, ->(value) { where(arel_table[:domain].matches("%#{value}%")) }

  delegate :is_admin, to: :user

  def upvoted_comments
    votes.where(voteable_type: 'Comment')
  end

  def upvoted_stories
    votes.where(voteable_type: 'Story')
  end

  def voted_on?(voteable)
    votes.where(voteable: voteable).any?
  end

  def local?
    domain.nil?
  end

  def bot?
    %w(Application Service).include? actor_type
  end

  def acct
    local? ? username : "#{username}@#{domain}"
  end

  def local_username_and_domain
    "#{username}@#{Rails.configuration.x.local_domain}"
  end

  def to_webfinger_s
    "acct:#{local_username_and_domain}"
  end

  def subscribed?
    subscription_expires_at.present?
  end

  def possibly_stale?
    last_webfingered_at.nil? || last_webfingered_at <= 1.day.ago
  end

  def keypair
    @keypair ||= OpenSSL::PKey::RSA.new(private_key || public_key)
  end

  def magic_key
    modulus, exponent = [keypair.public_key.n, keypair.public_key.e].map do |component|
      result = []

      until component.zero?
        result << [component % 256].pack('C')
        component >>= 8
      end

      result.reverse.join
    end

    (['RSA'] + [modulus, exponent].map { |n| Base64.urlsafe_encode64(n) }).join('.')
  end
end

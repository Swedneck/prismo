class GroupStory < ApplicationRecord
  belongs_to :group
  belongs_to :story
end

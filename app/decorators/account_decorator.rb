class AccountDecorator < Draper::Decorator
  delegate_all

  def to_s
    "@#{object.username}"
  end

  def path
    h.account_path(username: object.username)
  end

  def to_meta_tags
    {
      title: to_s,
      description: object.bio,
      og: {
        title: to_s,
        image: (object.avatar_url(:size_60) if object.avatar.present?)
      }
    }
  end
end

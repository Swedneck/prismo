class CommentDecorator < Draper::Decorator
  delegate_all

  def excerpt
    h.strip_tags(object.body).truncate(200)
  end

  def to_meta_tags
    {
      title: object.story.title,
      description: excerpt,
      og: {
        image: (object.story.thumb_url(:size_200) if object.story.thumb.present?)
      }
    }
  end
end

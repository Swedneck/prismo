class AccountsController < ApplicationController
  layout 'application'

  def show
    @stories = StoriesQuery.new.hot.page(params[:page])
  end
end

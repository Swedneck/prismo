class StoriesController < ApplicationController
  layout 'application'

  before_action :set_account_upvoted_story_ids

  def index
    @page_title = 'Hot stories'
    @stories = StoriesQuery.new.hot.page(params[:page])
  end

  def recent
    @page_title = 'Recent stories'
    @stories = StoriesQuery.new.recent.page(params[:page])
    render :index
  end

  def show
    set_account_upvoted_comment_ids

    @story = Story.find(params[:id])
    @comments = @story.comments.includes(:account).hash_tree
    @comment = Comments::Create.new(
      story_id: @story.id
    )
  end

  def new
    @story = Stories::Create.new
    authorize @story
  end

  def create
    authorize Stories::Create

    outcome = Stories::Create.run(
      title: params.fetch(:story)[:title],
      url: params.fetch(:story)[:url],
      description: params.fetch(:story)[:description],
      tag_list: params.fetch(:story)[:tag_list],
      account: current_account
    )

    if outcome.valid?
      redirect_to outcome.result, notice: 'Story has been created'
    else
      @story = outcome
      render :new
    end
  end

  def edit
    story = find_story
    authorize story

    @story = Stories::Update.new(
      story: story,
      url: story.url,
      title: story.title,
      tag_list: story.tag_names.join(', '),
      description: story.description,
    )
  end

  def update
    story = find_story
    authorize story

    outcome = Stories::Update.run(
      story: story,
      url: params.fetch(:story)[:url],
      title: params.fetch(:story)[:title],
      tag_list: params.fetch(:story)[:tag_list],
      description: params.fetch(:story)[:description],
      account: current_account
    )

    if outcome.valid?
      redirect_to outcome.result, notice: 'Story has been updated'
    else
      @story = outcome
      render :edit
    end
  end

  private

  def find_story
    Story.find(params[:id])
  end
end

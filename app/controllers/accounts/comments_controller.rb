class Accounts::CommentsController < ApplicationController
  layout 'application'

  before_action :set_account_upvoted_comment_ids

  def hot
    @account = find_account
    @page_title = "Hot comments by #{@account.decorate}"
    comments = CommentsQuery.new.hot
    comments = CommentsQuery.new(comments).by_account(@account)

    @comments = comments.page(params[:page])

    render :index
  end

  def recent
    @account = find_account
    @page_title = "Recent comments by #{@account.decorate}"
    comments = CommentsQuery.new.recent
    comments = CommentsQuery.new(comments).by_account(@account)

    @comments = comments.page(params[:page])

    render :index
  end

  private

  def find_account
    Account.find_by!(username: params[:username]).decorate
  end
end

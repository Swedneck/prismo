class Api::Ujs::CommentsController < Api::Ujs::BaseController
  before_action :set_account_upvoted_comment_ids

  def new
    story = Story.find(params[:story_id])

    @comment = Comments::Create.new
    @comment.story_id = params[:story_id]
    @comment.parent_id = params[:parent_id]

    render 'comments/new', layout: false
  end

  def create
    commentable = find_commentable
    authorize commentable, :comment?

    outcome = Comments::Create.run(
      parent_id: params.fetch(:comment)[:parent_id].presence,
      story_id: params.fetch(:comment)[:story_id],
      body: params.fetch(:comment)[:body],
      account: current_user.account
    )

    if outcome.valid?
      set_account_upvoted_comment_ids
      render 'comments/_comment', layout: false, locals: { comment: outcome.result }, status: :created
    else
      @comment = outcome
      render 'comments/new', layout: false, status: :bad_request
    end
  end

  def edit
    comment = find_comment
    authorize comment

    @comment = Comments::Update.new(
      comment: comment,
      body: comment.body
    )

    render 'comments/edit', layout: false
  end

  def update
    comment = find_comment
    authorize comment

    outcome = Comments::Update.run(
      comment: comment,
      body: params.fetch(:comment)[:body],
      account: current_account
    )

    if outcome.valid?
      render 'comments/_comment', layout: false, locals: { comment: outcome.result }, status: :created
    else
      @comment = outcome
      render 'comments/edit', layout: false, status: :bad_request
    end
  end

  def toggle_vote
    user_needed
    comment = find_comment
    authorize comment

    outcome = Comments::ToggleVote.run(comment: comment, account: current_user.account)
    set_account_upvoted_comment_ids

    render 'comments/_comment', layout: false, locals: { comment: outcome.result.voteable }
  end

  private

  def find_comment
    Comment.find(params[:id])
  end

  def find_commentable
    if params[:parent_id].present?
      Comment.find(params[:comment][:parent_id])
    else
      Story.find(params[:comment][:story_id])
    end
  end
end

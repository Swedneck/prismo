class ApplicationController < ActionController::Base
  include Pundit

  helper_method :current_account

  private

  def current_account
    current_user&.account&.decorate
  end

  def set_account_upvoted_comment_ids
    ids = user_signed_in? ? current_account.upvoted_comments.map(&:voteable_id) : []
    @account_upvoted_comment_ids = ids
  end

  def set_account_upvoted_story_ids
    ids = user_signed_in? ? current_account.upvoted_stories.map(&:voteable_id) : []
    @account_upvoted_story_ids = ids
  end
end

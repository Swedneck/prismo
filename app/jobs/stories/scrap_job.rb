require 'open-uri'

class Stories::ScrapJob < ApplicationJob
  queue_as :default

  def perform(story_id)
    story = Story.find(story_id)
    return if !story.url.present?

    meta = LinkThumbnailer.generate(story.url)
    story.touch(:scrapped_at)
    return if meta.images.empty?

    image_uri = meta.images.first.src
    story.update(thumb: open(image_uri))

    save_url_meta(story, meta)

    Stories::BroadcastChanges.run! story: story
  end

  private

  def save_url_meta(story, meta)
    video = meta.videos.last
    url_meta = story.url_meta || UrlMeta.new

    url_meta.update_attributes(
      title: meta.title,
      description: meta.description,
      video_id: video&.id,
      video_provider: video&.provider&.downcase,
      video_duration: video&.duration
    )

    story.update(url_meta: url_meta)
  end
end

class InitialStateSerializer < ActiveModel::Serializer
  attributes :meta, :accounts

  def meta
    store = {
      token: object.token,
      current_account_id: object.current_account&.id,
      site_settings: {
        site_title: object.site_settings.site_title,
        site_description: object.site_settings.site_description
      }
    }

    store
  end

  def accounts
    store = []

    store.push(
      Serializer.new(object.current_account, serializer: REST::AccountSerializer)
    ) if object.current_account

    store
  end
end

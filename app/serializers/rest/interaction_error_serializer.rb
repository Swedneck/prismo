class REST::InteractionErrorSerializer < ActiveModel::Serializer
  attribute :errors
  attribute :full_errors do
    object.errors.full_messages
  end
end

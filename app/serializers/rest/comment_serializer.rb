class REST::CommentSerializer < ActiveModel::Serializer
  include Rails.application.routes.url_helpers

  attribute :id
  attribute :body
  attribute :body_html
  attribute :story_id
  attribute :parent_id
  attribute :account_id
  attribute :votes_count
  attribute :is_voted
  attribute :children_count
  attribute :created_at

  has_one :account, serializer: REST::AccountSerializer
  has_one :story, if: :load_story?

  attribute(:depth) { object.depth_cached }
  attribute(:path) { comment_path(object) }

  def is_voted
    if @instance_options.key? :voted_comment_ids
      @instance_options[:voted_comment_ids].include? object.id
    elsif defined?(current_account) && current_account.present?
      ids = current_account.upvoted_comments.map(&:voteable_id)
      ids.include? object.id
    else
      false
    end
  end

  def load_story?
    return true if !@instance_options.key?(:exclude)

    !@instance_options[:exclude].include?(:story)
  end
end

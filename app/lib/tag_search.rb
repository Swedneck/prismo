class TagSearch
  include SearchObject.module(:paging)
  include SearchObject.module(:sorting)

  scope { Gutentag::Tag.all }

  sort_by :taggings_count
end

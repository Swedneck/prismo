class Stories::BroadcastChanges < ActiveInteraction::Base
  object :story

  def execute
    ActionCable.server.broadcast 'updates_channel', {
      event: 'stories.updated',
      data: ActiveModelSerializers::SerializableResource.new(story, serializer: REST::StorySerializer)
    }
  end
end

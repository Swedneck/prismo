class AddDepthCachedToComments < ActiveRecord::Migration[5.2]
  def change
    add_column :comments, :depth_cached, :integer, default: 0
  end
end

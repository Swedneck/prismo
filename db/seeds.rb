group = Group.create!(
  name: 'General',
  supergroup: true
)

account = Account.create!(
  username: 'admin',
  display_name: 'Site Admin'
)

User.create!(
  account: account,
  email: 'admin@example.com',
  password: 'TestPass',
  is_admin: true,
  confirmed_at: Time.zone.now
)

stories = [
  {
    account: account,
    title: 'Szokujące słowa minister: Facebook nie zapłacił w Polsce ani grosza podatku',
    url: 'http://m.superbiz.se.pl/wiadomosci-biz/szokujace-slowa-minister-facebook-nie-zaplacil-w-polsce-ani-gorsza-podatku_1055497.html',
    votes_count: 14,
    tag_names: ['seo', 'security'],
    groups: [group]

  }, {
    account: account,
    title: 'GDPR Hall of Shame',
    url: 'http://gdprhallofshame.com/',
    votes_count: 51,
    tag_names: ['security'],
    groups: [group]
  }, {
    account: account,
    title: 'Pony 0.22.0 Released',
    url: 'https://www.ponylang.org/blog/2018/05/0.22.0-released/',
    votes_count: 6,
    tag_names: ['release'],
    groups: [group]
  }, {
    account: account,
    title: 'WireGuard is available for OpenBSD',
    url: 'https://marc.info/?l=openbsd-ports&m=152712417729497&w=2',
    votes_count: 31,
    tag_names: ['networking', 'openbsd', 'security'],
    groups: [group]
  }
]

Story.create!(stories)

story = Story.first

comment1 = story.comments.create! account: account, body: 'Sample comment 1'
comment1_1 = comment1.children.create! account: account, body: 'Sample comment 1-1', story: story
comment1_2 = comment1.children.create! account: account, body: 'Sample comment 1-2', story: story

comment2 = story.comments.create! account: account, body: 'Sample comment 2'
comment2_1 = comment2.children.create! account: account, body: 'Sample comment 2-1', story: story
comment2_2 = comment2.children.create! account: account, body: 'Sample comment 2-2', story: story
comment2_2_1 = comment2_2.children.create! account: account, body: 'Sample comment 2-2-1', story: story

Comment.all.each do |comment|
  comment.cache_depth
  comment.cache_body
end

require 'sidekiq/web'

Rails.application.routes.draw do
  authenticate :user, lambda { |u| u.is_admin? } do
    mount Sidekiq::Web, at: 'admin/sidekiq', as: :sidekiq
  end

  devise_for :users, path: 'auth', controllers: {
    # omniauth_callbacks: 'auth/omniauth_callbacks',
    # sessions:           'auth/sessions',
    registrations:      'auth/registrations',
    # passwords:          'auth/passwords',
    # confirmations:      'auth/confirmations',
  }

  root to: 'stories#index'

  get '.well-known/host-meta', to: 'well_known/host_meta#show', as: :host_meta, defaults: { format: 'xml' }
  get '.well-known/webfinger', to: 'well_known/webfinger#show', as: :webfinger

  resource :account, path: '@:username' do
    root to: 'accounts/stories#hot'
    get :recent, to: 'accounts/stories#recent', as: :recent_stories

    resources :comments, only: [] do
      root to: 'accounts/comments#hot'
      get :recent, on: :collection, controller: 'accounts/comments'
    end
  end

  resources :stories do
    get :recent, on: :collection
  end

  resources :tags, param: :name, only: [] do
    resources :stories, path: '/', controller: 'tags/stories', only: [] do
      get '/', action: :hot, on: :collection
      get :recent, on: :collection
    end
  end

  resources :comments, only: [:index, :show] do
    get :recent, on: :collection
  end

  namespace :api do
    # Salmon
    post '/salmon/:id', to: 'salmon#update', as: :salmon

    namespace :ujs do
      resources :stories, only: [] do
        post :toggle_vote, on: :member
        post :scrap, on: :member
      end

      resources :comments, only: [:new, :create, :edit, :update] do
        post :toggle_vote, on: :member
      end
    end

    namespace :v1 do
      post 'tools/scrap_url', controller: :tools, action: :scrap_url
    end
  end

  namespace :settings do
    resource :profile, only: [:show, :update]
  end

  namespace :admin do
    resource :settings, only: [:edit, :update]
  end
end
